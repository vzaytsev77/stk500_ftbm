AvrUsb500 by Petka, вариант программатора AVR, доступный для простого изготовления.
Вариант с преобразователями уровней для сигналов ISP, проект в KiCAD прилагается.

Исходники собираются gcc-avr.

Начальная прошивка контроллера:

добавить эти строки в /etc/avrdude.conf

```
#!config

programmer
  id    = "stk500_by_petka";
  desc  = "AvrUsb500 by Petka, reset=!txd sck=!rts mosi=!dtr miso=!cts";
  type  = "serbb";
  reset = ~3;
  sck   = ~7;
  mosi  = ~4;
  miso  = ~8;
;

```

затем залить прошивку:
**avrdude -p atmega8 -c stk500_by_petka -P /dev/ttyUSB0 -i 10 -B 10 -b 9600 -U flash:w:atmega8.hex -U hfuse:w:0xC9:m -U lfuse:w:0x2F:m**

последующие заливки можно проще:
**avrdude -p atmega8 -c stk500_by_petka -P /dev/ttyUSB0 -U flash:w:atmega8.hex**


тема на форуме (откуда взяты исходники и схема):
http://electronix.ru/forum/index.php?showtopic=68372

исходники взял здесь:
http://electronix.ru/forum/index.php?showtopic=68372&st=900&p=1077360&#entry1077360